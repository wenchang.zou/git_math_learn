/**
 * Created by cherryzou on 2018/6/4.
 */
import req from './request';
import alert from './base';
const M={
   Req(url,types,success,data,error,async,uTtype){
     let xhr=new XMLHttpRequest(),vm=this;
     let uData=req.params(data);
     xhr.withCredentials = true;
     if(async == true) {
       xhr.onreadystatechange=function(){
         if(xhr.readyState==4){
           callback();
         }
       }
     }else{
       callback();
     }
     switch(types) {
       case "get":
         url += url.indexOf('?') == -1 ? '?' + uData : '&' + uData;
         xhr.open(types, req.urls + url, async);
         xhr.send(null);
         break;
       case "post":

         if(uTtype&& uTtype=='u'){
           url += url.indexOf('?') == -1 ? '?' + uData : '&' + uData
           xhr.open(types, req.urls + url, async);
            xhr.send(null);
         }else{
           xhr.open(types, req.urls + url, async);
           xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
           xhr.send(JSON.stringify(data));
         }


         break;
     }
     function callback(){
       if(xhr.status==200){
           if(JSON.parse(xhr.responseText).status=='fail'){
              alert.alertBox({
                type:"mini",
                msg:JSON.parse(xhr.responseText)["error_msg"]
              })
             return;
           }
           success && success(JSON.parse(xhr.responseText));
       }else{
         error && error(xhr.status)
       }
     }
   },
  sendCode(success,data,error){
     this.Req("/sendCode",'get',success,data,error,true);
  },
  logins(success,data,error){
    this.Req("/login",'get',success,data,error,true);
  },
  registers(success,data,error){
    this.Req("/regist",'post',success,data,error,true,'u');
  },
  isRegist(success,data,error){
    this.Req("/isRegist",'get',success,data,error,true);
  },
  getExcise(success,data,error){
    this.Req("/exercises/getExercises",'post',success,data,error,true,'u');
  },
  getAnswer(success,data,error){
    this.Req("/exercises/getAnswer",'post',success,data,error,true,'u');
  },
  classwork(success,data,error){
    this.Req("/exercises/classwork",'post',success,data,error,true,'u');
  },
  addErrorExercise(success,data,error){
    this.Req("/exercises/adderrorexercises",'post',success,data,error,true,'u');
  },
  errorExercise(success,data,error){
    this.Req("/exercises/errorexercises",'post',success,data,error,true,'u');
  },
  getSubjectList(success,data,error){
    this.Req("/subjects",'post',success,data,error,true);
  },
  getChapterList(success,data,error){
    this.Req("/chapters",'post',success,data,error,true);
  },
  getExerciseList(success,data,error){
    this.Req("/sections",'get',success,data,error,true);
  },
  getVideoList(success,data,error){
    this.Req("/getVideoInfo",'get',success,data,error,true);
  },
  recent(success,data,error){
    this.Req("/recentlyLearned",'post',success,data,error,true,'u');
  },
  getUserSetting(success,data,error){
    this.Req("/getUserSetting",'post',success,data,error,true,'u');
  },
  getUserInfo(success,data,error){
    this.Req("/getUserInfo",'post',success,data,error,true,'u');
  },
  noteCount(success,data,error){
    this.Req("/noteCount",'post',success,data,error,true,'u');
  },
  myNoteInfo(success,data,error){
    this.Req("/myNoteInfo",'post',success,data,error,true,'u');
  },
  editNote(success,data,error){
    this.Req("/editNote",'post',success,data,error,true,'u');
  },
  deleteNote(success,data,error){
    this.Req("/deleteNote",'post',success,data,error,true,'u');
  },
  addNote(success,data,error){
    this.Req("/addNote",'post',success,data,error,true,'u');
  },
  noteList(success,data,error){
    this.Req("/noteList",'post',success,data,error,true,'u');
  },
  setPassword(success,data,error){
    this.Req("/setPassword",'post',success,data,error,true,'u');
  }

}
export  default  M;
