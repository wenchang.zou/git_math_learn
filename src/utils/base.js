/*
 * @author cherry
 */
  const alerts={
      alertBox(opts){
        function AlertBox(opts){

            this.opts = opts || {};
            this.title = this.opts.title || '';
            this.msg = this.opts.msg || '';
            this.confirmMsg = this.opts.confirmMsg || '确定';
            this.cancelMsg = this.opts.cancelMsg || '取消';
            this.type = this.opts.type || 'default';
            this.hasMask = (this.opts.hasMask === false) ? false : true;
            this.uid = this.opts.uid || "#alertBox";
            this.confirm = this.opts.confirm || function(){};
            this.cancel = this.opts.cancel || function(){};
            this.callBack = this.opts.callBack || function(){};

        }
        AlertBox.prototype = {
            init: function(){
                var self = this;
                switch(self.type) {
                    case "default":
                        var html = '<div id="alertBox" class="alertBox"><div class="msg">${msg}</div><div class="layout">' +
                            '<div class="btn-cancel"><span>'+this.cancelMsg+'</span></div>' +
                            '<div class="btn-confirm"><span>'+this.confirmMsg+'</span></div></div></div>';
                        break;
                    case "alert":
                        var html = '<div id="alertBox" class="alertBox"><div class="msg">${msg}</div>' +
                            '<div class="layout wbox"><div class="btn-confirm sn-btn-c wbox-flex block"><a href="javascript:void(0)">'+this.confirmMsg+'</a></div></div></div>';
                        break;
                    case "mini":
                        var html = '<div id="alertBox" class="alertBox alertBoxBlack"><div class="msg">${msg}</div></div>';
                        break;
                    case 'inner':
                        var html = '<div id="alertBox" class="alertBox alertBox-inner">${msg}</div>';
                }
                var tpl = this.HTMLTemplate({
                    string: html,
                    para: {
                        msg: self.msg
                    }
                });
                self.render(tpl);
            },
            render: function(_htmlTpl){
                var self = this;
                var body = document.querySelector("body");
                !document.querySelector(self.uid) && body.insertAdjacentHTML('beforeend', _htmlTpl);
                (typeof self.callBack == "function") && self.callBack();
                self.hasMask && self.mask(body);
                self.setPos(document.querySelector(self.uid));
            },
            mask: function(body){
                var maskEl = document.createElement("div");
                maskEl.id = 'tempMask';
                var _height = this.getHeight();
                body.appendChild(maskEl);
                document.querySelector("#tempMask").style.cssText = ";height:" + _height + "px;width:" + document.documentElement.offsetWidth + "px;";
            },
           $(id){
              return document.querySelector(id);
           },
            HTMLTemplate:function(opts){
                var o = opts.para || {};
                var html = [opts.string].join('');
                if (o) {
                    for (var i in o) {
                        //替换掉${msg}
                        html = html.replace(/\$\{(\w+)\}/g, o[i]); // "\"转义符,先匹配
                    }
                    return html;
                }
                return html;
            },
            setPos: function(El){
                var self = this;
                var scrollTop = this.scrollTop();
                El.style.cssText = ";top:" + (scrollTop + window.innerHeight/2 - El.offsetHeight/2) + "px;left:" + (document.documentElement.offsetWidth/2 - El.offsetWidth/2) + "px;display:block;";
                if(self.type == "mini"){
                    document.querySelector("#tempMask").style.opacity = 0;

                    setTimeout(function(){
                        document.body.removeChild(self.$("#alertBox"));
                        document.body.removeChild(self.$("#tempMask"));
                    },2000);
                    return;
                }
                self.addEvent(El);
            },
            addEvent: function(El){
                var self = this;
                self.$(".btn-confirm").addEventListener("click", function(e){
                    self.confirm(El);
                    self.reset(El);
                    e.preventDefault();
                });
                if(self.type != "alert"){
                    self.$(".btn-cancel").addEventListener("click", function(e){
                        self.cancel(El);
                        self.reset(El);
                        e.preventDefault();
                    });
                }
            },
            reset: function(El){
              var self=this;
                if(self.$("#tempMask")){
                    this.remove.call(self.$("#tempMask"));
                }
                this.remove.call(El);
                if(this.type != "mini"){
                    this.die(El);
                }
            },
            die: function(El){
              var self=this;
              self.$(El.querySelector(".btn-confirm")).removeEventListener("click");
                if(self.type != "alert"){
                    self.$(El.querySelector(".btn-cancel")).removeEventListener("click");
                }
            },
            getHeight: function(){
                var height = document.documentElement.offsetHeight || document.body.offsetHeight;
                if(window.innerHeight > height){
                    height = window.innerHeight;
                }
                return height;
            },
            scrollTop: function(){
                return document.documentElement.scrollTop || document.body.scrollTop;
            },
            remove: function(){
                return this.parentNode.removeChild(this);
            }


        };
        new AlertBox(opts).init();
    }
};
export default alerts;
