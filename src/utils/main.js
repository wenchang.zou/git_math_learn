/**
 * Created by cherryzou on 2018/6/4.
 */
const methods={
   setIntervals(wait,fn,bn){
      var i=0, runing,
         timer=function(){
           var r=wait-i;


            if(r==0){
              bn && bn();
              clearInterval(runing);
            }else{
              i++;
              fn && fn(r);
            }
         };
        runing=window.setInterval(timer,1000);
  }
}
export  default  methods;
