/**
 * Created by cherryzou on 2018/6/25.
 */
const Req={
    urls:"http://study24.cn",
    params(data){
      let arr=[],str='';
      for(let k in data){
        arr.push(k+"="+data[k]);
      }
      str=arr.join("&");
      return str;
    },
    ajax(opts){
        let xhr=new XMLHttpRequest(),vm=this;
        opts.data=this.params(opts.data);
      xhr.withCredentials = true;
      if(opts.async == true) {
            xhr.onreadystatechange=function(){
              if(xhr.readyState==4){
                callback();
              }
            }
        }else{
          callback();
        }
        switch(opts.types){
            case "get":
              opts.url += opts.url.indexOf('?') == -1 ? '?' +opts.data : '&' + opts.data;
              xhr.open(opts.types, vm.urls+opts.url, opts.aysnc);
              xhr.send(null);
            break;
            case "post":
              xhr.open(opts.types, vm.urls+opts.url, opts.aysnc);
              xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
              xhr.send(opts.data);
            break;
          }


          function callback(){
            if(xhr.status==200){
                opts.success && opts.success(JSON.parse(xhr.responseText));
            }else{
              opts.error && opts.error(xhr.status)
            }
          }
    }
}
export default Req;
