import Vue from 'vue'
import Router from 'vue-router'

import register from '@/pages/register'
import login from '@/pages/login'
import forget from '@/pages/forget'
import index from '@/pages/index'
import loading from '@/pages/loading'
import knowledge from '@/pages/knowlege'
import personCenter from '@/pages/personalCenter'
import setting from '@/pages/setting'
import myNote from '@/pages/myNote'
import myMistake from '@/pages/myMistake'
import recent from '@/pages/recentLearning'
import description from '@/pages/mistakeDesc'
import exersice from '@/pages/exersice'
import charpter from '@/pages/charpter'
import pwdLogin from '@/pages/pwdLogin'
import editNote from '@/pages/editNote'
import setPassword from '@/pages/setPassword'
import about from '@/pages/about'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/forget',
      name: 'forget',
      component: forget
    },
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/loading',
      name: 'loading',
      component: loading
    },
    {
      path: '/knowledge',
      name: 'knowledge',
      component: knowledge
    },
    {
      path: '/personCenter',
      name: 'personCenter',
      component: personCenter
    },
    {
      path: '/setting',
      name: 'setting',
      component: setting
    },
    {
      path: '/myNote',
      name: 'myNote',
      component: myNote
    },
    {
      path: '/myMistake',
      name: 'myMistake',
      component: myMistake
    },
    {
      path: '/recent',
      name: 'recent',
      component: recent
    },
    {
      path: '/description',
      name: 'description',
      component: description
    },
    {
      path: '/charpter',
      name: 'charpter',
      component: charpter
    },
    {
      path: '/exersice',
      name: 'exersice',
      component: exersice
    },
    {
      path: '/pwdLogin',
      name: 'pwdLogin',
      component: pwdLogin
    },
    {
      path: '/editNote',
      name: 'editNote',
      component: editNote
    },
    {
      path: '/setPassword',
      name: 'setPassword',
      component: setPassword
    },
    {
      path: '/about',
      name: 'about',
      component: about
    }
  ]
})


